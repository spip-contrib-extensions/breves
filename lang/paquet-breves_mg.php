<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-breves?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// B
	'breves_description' => 'Ny sombim-baovao dia filazana fohy, tsy voatonona ny mpanoratra',
	'breves_slogan' => 'Fitantanana ny sombim-baovao ao amin’i SPIP',
];
