# Plugin Brèves

![breves](./prive/themes/spip/images/breve-xx.svg)

Ce plugin gère des brèves : des informations courtes sans auteur.


**Documentation officielle**\
https://contrib.spip.net/5473
